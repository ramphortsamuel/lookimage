<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use App\Entity\Picture;
use App\Entity\Category;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        // équivalent à un SELECT * FROM pictures
        $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findBy(['is_validated' => true]);
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        // page par page
        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $resultsPictures, // Requête contenant les données à paginer (ici nos articles)
            $page === 0 ? 1 : $page,
            12 // Nombre de résultats par page
        );

        return $this->render('home/index.html.twig', [
            'pictures' => $pictures,
            'categories' => $categories
            ]);
    }

    /**
     * @Route("/category/{id}", name="pictures_by_category")
     */
    public function picturesByCategory($id, PaginatorInterface $paginator, Request $request)
    {
        // dd($id);
        // dump($id);
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        $query = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'category' => $category,
            'is_validated' => true
        ]);

        if(!$category){
            // Si aucun article n'est trouvé, nous créons une exception
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        }

        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $category->getPictures(), // Requête contenant les données à paginer (ici nos articles)
            $page === 0 ? 1 : $page,
            6 // Nombre de résultats par page
        );

        // Si l'article existe nous envoyons les données à la vue
        return $this->render('home/picturesByCategory.html.twig', [
            'category' => $category,
            'pictures' => $pictures
            ]);
    }

    /**
     * @Route("/show/picture/{id}", name="show_picture", requirements={"id"="\d+"})
     */
    public function showPicture($id, PaginatorInterface $paginator, Request $request)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
        $pictures = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'category' => $picture->getCategory(),
            'is_validated' => true
        ]);

        if(!$picture){
            // Si aucun article n'est trouvé, nous créons une exception
            throw $this->createNotFoundException('L\'image n\'existe pas');
        }

        $page = $request->query->getInt('page', 1);
        $photos = $paginator->paginate(
            $pictures, // Requête contenant les données à paginer (ici nos articles)
            $page === 0 ? 1 : $page,
            3 // Nombre de résultats par page
        );
        
        return $this->render('home/showPicture.html.twig',[
            'picture' => $picture,
            'photos' => $photos

        ]);
    }
}

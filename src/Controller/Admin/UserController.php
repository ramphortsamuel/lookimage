<?php

namespace App\Controller\Admin;

use App\Form\UploadUserType;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AbstractController
{
    /**
     * @Route("/admin/users", name="admin_users")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $query = $this->getDoctrine()->getRepository(User::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $users = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            25
        );

        return $this->render('admin/user/index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/admin/users/delete/{id}", name="admin_users_delete")
     */
    public function delete($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        
        if(!$user) {
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas');
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($user);
        $doctrine->flush();

        $this->addFlash('success', 'L\'utilisateur a bien été supprimée');

        return $this->redirectToRoute('admin_users');
    }
    
    /**
     * @Route("/admin/users/edit/{id}", name="admin_users_edit")
     */
    public function edit($id, Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        
        if(!$user) {
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas');
        }

        $formEditUser = $this->createForm(UploadUserType::class, $user);

        $formEditUser->handleRequest($request);

        if ($formEditUser->isSubmitted() && $formEditUser->isValid()) {

            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($user);
            $doctrine->flush();

            // créer un message flash
            $this->addFlash('success', 'L\'utilisateur a bien été modifié');

            // redirection
            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/user/edit.html.twig', [
            'formEditUser' => $formEditUser->createView(),
        ]);
    }
}
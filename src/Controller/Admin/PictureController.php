<?php

namespace App\Controller\Admin;
use App\Form\UploadPictureType;

use App\Entity\Picture;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class PictureController extends AbstractController
{
    /**
     * @Route("/admin/pictures", name="admin_pictures")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $query = $this->getDoctrine()->getRepository(Picture::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $pictures = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            25
        );

        return $this->render('admin/picture/index.html.twig', [
            'pictures' => $pictures
        ]);
    }

    /**
     * @Route("/admin/pictures/delete/{id}", name="admin_pictures_delete")
     */
    public function delete($id)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
        
        if(!$picture) {
            throw $this->createNotFoundException('L\'image n\'existe pas');
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        $this->addFlash('success', 'La photo a bien été supprimée');

        return $this->redirectToRoute('admin_pictures');
    }

    /**
     * @Route("/admin/pictures/edit/{id}", name="admin_pictures_edit")
     */
    public function edit($id, Request $request)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
        
        if(!$picture) {
            throw $this->createNotFoundException('L\'image n\'existe pas');
        }

        $formEditPicture = $this->createForm(UploadPictureType::class, $picture, [
            'validation_groups' => 'edit_picture'
        ]);
        $formEditPicture->handleRequest($request);

        if ($formEditPicture->isSubmitted() && $formEditPicture->isValid()) {

            $picture->setCreatedAt(new \DateTimeImmutable());
            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            // créer un message flash
            $this->addFlash('success', 'La photo a bien été modifiée');

            // redirection
            return $this->redirectToRoute('admin_pictures');
        }

        return $this->render('admin/picture/edit.html.twig', [
            'formEditPicture' => $formEditPicture->createView(),
        ]);

    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Picture;
use App\Entity\Category;
use App\Form\UploadCategoryType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;


class CategoryController extends AbstractController
{
    /**
     * @Route("/admin/categories", name="admin")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $query = $this->getDoctrine()->getRepository(Category::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $categories = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            20
        );

        return $this->render('admin/index.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/categories/new", name="admin_categories_new")
     */
    public function new(Request $request)
    {
        // Instancie l'entité "Picture"
        $category = new Category();

        // Premier paramètre : Le formulaire dont ont a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formNewCategory = $this->createForm(UploadCategoryType::class, $category);
        $formNewCategory->handleRequest($request);

        // Vérifie si le formulaire est envoyé et valide
        if ($formNewCategory->isSubmitted() && $formNewCategory->isValid()) {

            // gen slug
            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($category);
            $doctrine->flush();

            // créer un message flash
            $this->addFlash('success', 'Nouvelle catégorie ajoutée !');

            // redirection
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/new.html.twig', [
            'formNewCategory' => $formNewCategory->createView()
        ]);
    }

    /**
     * @Route("/admin/categories/edit/{id}", name="admin_categories_edit")
     */
    public function edit($id, Request $request)
    {
        // selection en BDD
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        if (!$category) {
            throw $this->createForm(UploadCategoryType::class, $category);
        }

        $formEditCategory = $this->createForm(UploadCategoryType::class, $category);
        $formEditCategory->handleRequest($request);

        if ($formEditCategory->isSubmitted() && $formEditCategory->isValid()) {

            $category->setSlug((new AsciiSlugger())->slug(strtolower($category->getName())));

            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($category);
            $doctrine->flush();

            // créer un message flash
            $this->addFlash('success', 'Modifications enregistrées !');

            // redirection
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/edit.html.twig', [
            'formEditCategory' => $formEditCategory->createView(),
        ]);
    }

    /**
     * @Route("/admin/categories/delete/{id}", name="admin_categories_delete")
     */
    public function delete($id)
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        if (!$category) {
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($category);
        $doctrine->flush();

        $this->addFlash('success', 'La catégorie a bien été supprimée');

        return $this->redirectToRoute('admin');
    }

}
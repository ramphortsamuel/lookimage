<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Mmo\Faker\PicsumProvider;
use Faker;
use Symfony\Component\HttpFoundation\File\File;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{
    
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            UserFixtures::class
        ];
    }
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $faker->addProvider(new PicsumProvider($faker));

        for ($i = 0; $i <= 100; $i++) {
            $image = $faker->picsum('./public/uploads/images/photos', random_int(1152, 2312), random_int(864, 1736));

            $category = $this->getReference('category_' . random_int(0, 10));

            // recup ref user aléatoirement
            $user = $this->getReference('user_' . random_int(0, 10));

            $picture = new Picture();
            $picture->setDescription($faker->sentence(26));
            $picture->setTags($faker->word);
            $picture->setCreatedAt($faker->dateTimeBetween('- 4 years'));
            $picture->setUpdatedAt($faker->dateTimeBetween('- 2 years'));
            $picture->setCategory($category);
            $picture->setUser($user);

            // gestion de l'image
            $picture->setImageFile(new File($image));
            $picture->setImage(str_replace('./public/uploads/images/photos\\', '', $image));

            $manager->persist($picture);
        }
        $manager->flush();
    }
}

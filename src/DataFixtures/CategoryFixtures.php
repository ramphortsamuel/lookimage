<?php

namespace App\DataFixtures;
use Faker;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use Symfony\Component\String\Slugger\AsciiSlugger;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // instanciation de Faker
        $faker = Faker\Factory::create('fr_FR');
        // creation d'une boucle for (nb d'éléments)
        for ($i = 0; $i <= 10; $i++)
        {
            $category = new Category();
            $category->setName($faker->colorName);
            $category->setSlug((new AsciiSlugger())->slug(strtolower($faker->colorName)));

            $this->addReference('category_'. $i, $category);
            $manager->persist($category);

        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
